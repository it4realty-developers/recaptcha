var widgetId, response = true;
var onloadCallback = function() {
  
  var verifyCallback = function() {
    response = false;
    jQuery('.site_captcha-error').hide();  
  };
  //перебираем все елемены рекапчи
  jQuery(".g_recaptcha").each(function(i, elm) {
    //получаем id рекапчи
    var gid = elm.id;
    //рендерим рекапчу
    widgetId = grecaptcha.render(gid, {
      "sitekey" : Drupal.settings.site_captcha,
      "theme" : "light",
      "type": "image",
      'callback' : verifyCallback 
    });
    
    //проверка перед отправкой формы
    jQuery("#"+jQuery(elm).parents("form").attr("id")).on("submit", function(e){
      if(response) {
        e.preventDefault();
        jQuery('#'+elm.parentElement.id + ' .site_captcha-error').show();
      }
      if (e.isDefaultPrevented()) {
        return false;
      } else {
        return true;
      }
    });
  
  });
};

<?php

/**** Настройки валют ****/
function site_captcha_setting_form() {
  
  $form['site_captcha_recapcha'] = array(
	'#type' => 'fieldset',
  );
    
  //ключ для вставки в html
  $form['site_captcha_recapcha']['site_captcha_recapcha_key'] = array(
	'#type' => 'textfield',
	'#title' => t('Key'),
	'#default_value' => variable_get('site_captcha_recapcha_key', ''),
	'#description' => t('The key to building a google captcha')
  );
    
  //секретный ключ 
  $form['site_captcha_recapcha']['site_captcha_recapcha_secret_key'] = array(
	'#type' => 'textfield',
	'#title' => t('Secret key'),
	'#default_value' => variable_get('site_captcha_recapcha_secret_key', ''),
	'#description' => t('This key is needed for communications between your site and Google. Do not disclose this information')
  );
    
  //дополнительная инфомрация по recapcha
  $form['site_captcha_recapcha']['recapcha_info'] = array(
	  '#type'=> 'item',
	  '#markup'=> '<p>
		<label>'.t('Additional Information:').'</label>
		<a href="https://www.google.com/recaptcha/admin" target="_blank">'.t('Personal account').'</a>&nbsp;|&nbsp;
		<a href="https://developers.google.com/recaptcha/intro" target="_blank">'.t('Documentation').'</a>&nbsp;|&nbsp;
		<a href="http://www.google.com/recaptcha/api2/demo" target="_blank">'.t('Demo version').'</a>
	  </p>',
  );
    
  return system_settings_form($form);
  
}

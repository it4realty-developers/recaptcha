<?php

/**
 * Реализация hook_permission
 */ 
function site_captcha_permission() {
    
  return array(
	'gcaptcha settings' => array(
	  'title' => t('google captcha settings')
	),
  );
  
}

/**
 * Реализация hook_menu
 */
function site_captcha_menu() {
  
  $items = array();

  //настроечная форма - настройки валюты
  $items['adminmenu/gcaptcha'] = array(
	'title' => 'Google Captcha', 
	'page callback' => 'drupal_get_form',
	'page arguments' => array('site_captcha_setting_form'),
	'access arguments' => array('gcaptcha settings'),
	'file' => 'site_captcha.admin.inc',
	'file path' => drupal_get_path('module', 'site_captcha') . '/includes',  
	'type' => MENU_NORMAL_ITEM,
	'menu_name' => 'menu-adminmenu',
	'weight' => 22
  );
  // добавляем иконку
  pr_admin_f_api_adminmenu_icon_set('adminmenu/gcaptcha', 'fa fa-ban');
  
  return $items;
}